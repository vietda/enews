<%-- 
    Document   : categoryPageContent
    Created on : Oct 16, 2017, 8:52:24 PM
    Author     : Viet Diep
--%>

<div id="content">
    <div id="left" class="boxborder"><jsp:include page="inc/left.jsp"/></div>
    <div id="middle"><jsp:include page="inc/middle.jsp"/></div>
    <div id="right"><jsp:include page="../common/advertisement.jsp"/></div>
</div>
