<%-- 
    Document   : left
    Created on : Oct 25, 2017, 2:29:14 PM
    Author     : Viet Diep
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%--
<h4 class="blocksubhead">
    <s:if test="category.parentCategoryName != null">
        ${category.parentCategoryName} >
    </s:if>
    ${category.categoryName}
</h4>
--%>

<s:iterator value="postsView" status="status">
    <s:url action="news" var="linkView">
        <s:param name="postid" value="postID"/>
    </s:url>
    <s:if test="#status.first">
        <div class="folder-hot-news">
            <p>
                <s:if test="avatarImage != ''">
                    <a href="${linkView}">
                        <img class="folder-hot-image" src="${avatarImage}" onerror="this.onerror=null; this.style.display='none';"/>
                    </a>
                </s:if>
            </p>
            <h2 class="hotnews-top1-title"><a href="${linkView}">${title}</a></h2>
            <p class="datetime">${createdDTM}</p>
            <p>${shortDescription}</p>
        </div>
    </s:if>
    <s:else>
        <div class="folder-news">
            <p>
                <s:if test="avatarImage != ''">
                    <a href="${linkView}">
                        <img class="folder-news-image" src="${avatarImage}" onerror="this.onerror=null; this.style.display='none';"/>
                    </a>
                </s:if>
            </p>
            <p>
                <a href="${linkView}" class="folder-topnews-title">${title}</a>
            </p>
            <p class="datetime">
                ${createdDTM}
            </p>
            <p>${shortDescription}</p>
        </div>
    </s:else>
</s:iterator>

<s:if test="otherPostsView.size() > 0">
    <br class="clear"/>
    <h4 class="blocksubhead">Tin khác</h4>
    <div class="foldernews-older-list">
        <div class="newnews">
            <s:iterator value="otherPostsView">
                <s:url action="news" var="linkView">
                    <s:param name="postid" value="postID"/>
                </s:url>
                <p>+ <a href="${linkView}">${title}</a> <span class="datetime">- ${updatedDTM}</span></p>
            </s:iterator>
        </div>
    </div>
</s:if>