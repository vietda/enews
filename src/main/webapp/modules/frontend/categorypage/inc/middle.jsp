<%-- 
    Document   : middle
    Created on : Oct 26, 2017, 11:04:49 PM
    Author     : Viet Diep
--%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="boxtype1-title">
    Tin đọc nhiều nhất
</div>
<div class="boxtype1-content newnews">
    <s:iterator value="otherPostsView">
        <s:url action="news"  var="linkView">
            <s:param name="postid" value="postID"/>
        </s:url>
        <p><a href="${linkView}">${title}</a> <span class="datetime">- ${updatedDTM}</span></p>
    </s:iterator>   
</div>

<br />

<div class="boxtype1-title">
    Tin nổi bật
</div>
<div class="boxtype1-content newnews">
    <s:iterator value="otherPostsView">
        <s:url action="news" var="linkView">
            <s:param name="postid" value="postID"/>
        </s:url>
        <p><a href="${linkView}">${title}</a> <span class="datetime">- ${updatedDTM}</span></p>
    </s:iterator>
</div>
