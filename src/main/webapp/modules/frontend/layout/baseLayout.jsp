<%-- 
    Document   : baseLayout
    Created on : Sep 27, 2017, 8:54:18 PM
    Author     : vietdiep
--%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ page pageEncoding="UTF-8" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Báo điện tử</title>

        <!--Include CSS-->
        <link href="public/css/default.css" rel="stylesheet" type="text/css" />
        <link href="public/css/fwblueprint.css" rel="stylesheet" type="text/css" />

        <!--Include JS-->
        <script type="text/javascript" src="public/js/checkinput.js"></script>
        <script type="text/javascript" src="public/js/jquery-1.4.4.min.js"></script>
    </head>

    <body>
        <div id="container">
            <tiles:insertAttribute name="header" />
            
            <div class="clear"></div>
            <tiles:insertAttribute name="body" />
            <div class="clear"></div>

        </div>
        <tiles:insertAttribute name="footer" />
    </body>
</html>

