<%-- 
    Document   : middle
    Created on : Oct 23, 2017, 9:04:54 AM
    Author     : Viet Diep
--%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="box-newnews">
    <h4 class="titlesmall">Tin mới nhất</h4>
    <s:iterator value="lastedPublishedPostsView">
        <s:url action="news" var="linkView">
            <s:param name="postid" value="postID"/>
        </s:url>
        <p><a href="${linkView}">${title}</a> <span class="datetime">${updatedDTM}</span></p>
    </s:iterator>
</div>

<br />
<div class="clear"></div>
<div class="boxtype1-title">
	Tin nổi bật
</div>
<div class="boxtype1-content newnews">
    <s:iterator value="highViewPostsView">
        <s:url action="news" var="linkView">
            <s:param name="postid" value="postID"/>
        </s:url>
        <p><a href="${linkView}">${title}</a> <span class="datetime"> ${updatedDTM}</span></p>
    </s:iterator>
</div>
