<%-- 
    Document   : hotnews
    Created on : Oct 16, 2017, 10:16:09 PM
    Author     : Viet Diep
--%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="hotnews-top1">
    <s:url action="news" var="linkView">
        <s:param name="postid" value="hotPostTop1View.postID"/>
    </s:url>
    <a href="${linkView}">
        <s:if test="hotPostTop1View.avatarImage != ''">
            <img class="hotnews-top1-image" src="${hotPostTop1View.avatarImage}" onerror="this.onerror=null; this.src='';">
        </s:if>
    </a>
    <h2 class="hotnews-top1-title">
        <a href="${linkView}">${hotPostTop1View.title}</a>
    </h2>
    <p class="datetime"> ${updatedDTM}</p>
    <p>${hotPostTop1View.shortDescription}</p>
    <br class="clear"/>
</div>

<s:iterator value="hotPostTop234View">
    <s:url action="news" var ="linkView">
        <s:param name="postid" value="postID"/>
    </s:url>
    <div id="hotnews-top234">
        <div id="hotnews-top234-sub">
            <p>
                <a href="${linkView}">
                        <img class="hotsubimage" src="${avatarImage}" onerror="this.onerror=null; this.src='';">
                </a>
            </p>
            <p class="hotnews-top234-title">
                <a href="${linkView}">${title}</a>
            </p>
        </div>
    </div>
</s:iterator>
<br class="clear"/>