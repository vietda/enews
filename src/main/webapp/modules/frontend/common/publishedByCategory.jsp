<%-- 
    Document   : publishedByCategory
    Created on : Oct 19, 2017, 2:41:31 PM
    Author     : Viet Diep
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div id="folder-detail">
    <s:iterator value="postsPublishedByCategoryView" status="status">
        <s:url action="news" var="linkView">
            <s:param name="postid" value="postID"/>
        </s:url>
        <s:if test="#status.first">
            <div id="folder-topnews">
                <s:if test=" avartarImage != ''">
                    <a href="${linkView}"><img class="foldertopimage" src="${avatarImage}" onerror="this.onerror=null; this.src='';" width="100"></a>
                    </s:if>
                <p>
                    <a href="${linkView}" class="folder-topnews-title">${title}</a>
                </p>
                <p><span class="datetime">${updatedDTM} </span></p>
                <p>
                    ${shortDescription}
                </p>
                <span class="relationposts">
                    <s:iterator value="postsPublishedByCategoryView">
                        <s:url action="news" var="linkView">
                            <s:param name="postid" value="postID"/>
                        </s:url>
                        <p>&gt; <a href="${linkView}">${title}</a></p>
                    </s:iterator>
                </span>
            </div>
        </s:if>
        <s:else>
            <div class="folder-othernews">
                <p>
                    <a href="${linkView}">${title}</a> <span class="datetime">${updatedDTM} </span>
                </p>
            </div>
        </s:else>
    </s:iterator>
    <div class="clear"></div>
</div>