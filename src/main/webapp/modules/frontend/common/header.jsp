<%-- 
    Document   : header
    Created on : Sep 27, 2017, 9:05:30 PM
    Author     : vietdiep
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<div id="top">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <div style="height: 60px;padding-top: 16px"><img src="public/images/logo.gif" alt="" width="171" height="41"></div>
    <div class="navi">
        <div class="menu">
            <ul>
                <li>
                    <span><a href="home"><font color="#FFFFFF">Trang chủ</font></a></span>
                </li>

                <s:iterator value="categories">
                    <s:url var = "categoryLink" action = "category">
                        <s:param name= "categoryid" value= "categoryID"/>
                    </s:url>
                    <li><span <s:if test="childCategories.size()>0"> class="parent"</s:if>><a href="${categoryLink}">${categoryName}</a></span>
                        <ul>
                            <s:iterator value="childCategories" status="childStatus">
                                <s:url var="categoryLink" action="category">
                                <s:param name="categoryid" value="categoryID"/>
                                </s:url>
                                <s:if test="childStatus.last">
                                    <li class="nonbor"><span><a href="${categoryLink}">${categoryName}</a></span></li>
                                </s:if>
                                <s:else>
                                    <li><span><a href="${categoryLink}">${categoryName}</a></span></li>
                                </s:else>
                            </s:iterator>
                        </ul>
                    </li>
                </s:iterator>
                <li>
                    <a href="<s:url action="search"/>">Tìm kiếm</a>
                </li>
            </ul>
        </div>
    </div>
</div>