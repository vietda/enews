<%-- 
    Document   : baseLayout
    Created on : Nov 14, 2017, 12:04:32 PM
    Author     : Viet Diep
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Báo điện tử</title>
        <link href="../public/css/backend.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../public/css/function.js"></script>
        <script type="text/javascript" src="../public/css/checkinput.js"></script>
        <script type="text/javascript" src="../public/css/editor/ckeditor.js"></script>
        <script type="text/javascript" src="../public/css/jquery-1.4.4.min.js"></script>
        <link href="../public/css/editor/ckeditor.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="container">
            <div id="top"><tiles:insertAttribute name="header" /></div>
            <div class="clear"></div>
            <tiles:insertAttribute name="body" />
            <div class="clear"></div>
            <div id="footer"><tiles:insertAttribute name="footer" /></div>
        </div>
    </body>
</html>