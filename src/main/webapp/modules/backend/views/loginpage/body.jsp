<%-- 
    Document   : body
    Created on : Nov 15, 2017, 3:19:08 PM
    Author     : Viet Diep
--%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="content" class="showgrid">
    <s:form action="login" namespace="backend" method="post">
        <table align="center" cellpadding="0" cellspacing="0" class="table">
            <tr>
                <th colspan="2" align="center" class="head">Login</th>
            </tr>
            <s:if test="errorMessage.size != null">
                <tr><td colspan="2">
                        <span class="errorMessage">
                            <s:property value="errorMessage" />
                        </span>
                    </td>
                </tr>
            </s:if>
            <tr>
                <td width="91">Username</td>
                <td><s:textfield theme="simple" name="userName"/></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><s:password theme="simple" name="password"/></td>
            </tr>
            <tr>
                <td>Code</td>
                <td><s:textfield theme="simple" name="code"/></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <div>
                        <span class="seccode"><s:property value="scode"/></span>
                        <div style="opacity:0; height: 100%;position: absolute;top: 0px;width: 140px; background-color: #0000B9"></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input class="button" type="submit" name="button" id="button" value="Login" /></td>
            </tr>
        </table>
    </s:form>
</div>