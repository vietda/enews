<%-- 
    Document   : menu
    Created on : Nov 22, 2017, 10:01:13 AM
    Author     : Viet Diep
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@taglib prefix="s" uri="/struts-tags"%>
<div class="boxtype1-title">
    Users
</div>
<div class="boxtype1-content">
    <ul class="listcategory">
        <li>
            <s:url action="admin" namespace="/backend" var="linkUsers"/>
            <a href="${linkUsers}">+ Users</a>
        </li>
        <li>
            <s:url action="adduser" namespace="/backend" var="linkAddUser"/>
            <a href="${linkAddUser}">+ Add User</a>
        </li>
    </ul>
</div>

<br />
<div class="boxtype1-title">
    Categories
</div>
<div class="boxtype1-content">
    <ul class="listcategory">
        <li>
            <s:url action="categories" namespace="/backend" var="linkCategories"/>
            <a href="${linkCategories}">+ Categories</a>
        </li>
        <li>
            <s:url action="addcategory" namespace="/backend" var="linkAddCategory"/>
            <a href="${linkAddCategory}">+ Add Category</a>
        </li>
    </ul>
</div>

<br />

<div class="boxtype1-title">
    Statistic
</div>
<div class="boxtype1-content">
    <ul class="listcategory">
        <li>
            <s:url action="totalviewstats" namespace="/backend" var="linkTotalViewStats"/>
            <a href="${linkTotalViewStats}">+ Total View Stats</a>
        </li>
        <li>
            <s:url action="averageviewstats" namespace="/backend" var="linkAverageViewStats"/>
            <a href="${linkAverageViewStats}">+ Average Stats</a>
        </li>
    </ul>
</div>

<br />

<s:url action="profile" namespace="/backend" var="linkProfile"></s:url>
<a href="${linkProfile}">
    <div class="boxtype1-title">
        Profile
    </div>
</a>