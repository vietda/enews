/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function toggleCheckAll(field, objCheckAll, isCheckAll) {
    if (!field)
        return;
    if (isCheckAll == true) {
        setCheck(field, true);
        setCheck(objCheckAll, true);
    } else {
        setCheck(field, false);
        setCheck(objCheckAll, false);
    }
}

function unCheckCheckAll(field, status) {
    if (!field)
        return;
    if (status.checked == false) {
        setCheck(field, false);
    }
}
function setCheck(obj, value) {
    if (obj.length) {
        for (i = 0; i < obj.length; i++) {
            obj[i].checked = value;
        }
    } else
        obj.checked = value;
}

function poptastic(url, width, height)
{
    var left = (screen.width/2)-(width/2);
    var top = (screen.height/2)-(height/2);
    newwindow=window.open(url,'name','height=' + height + ',width=' + width + ', top=' + top + ',left=' + left + ',resizable=yes,scrollbars=yes,status=yes');
    if (window.focus) {
        newwindow.focus()
    }
}

function modalWindow(url, width, height)
{
    var left = (screen.width/2)-(width/2);
    var top = (screen.height/2)-(height/2);
    //newwindow=window.showModalDialog(url,'name','dialogHeight:' + height + 'px;dialogWidth:' + width + 'px; center:yes; resizable:yes');
    returnValue = window.showModalDialog(url, 'name', 'dialogWidth=575px;dialogHeight=570px;resizable=yes;help=no;unadorned=yes');
//    if (window.focus) {
//        newwindow.focus()
//        }
}

function getCountSelectedCheckbox(buttonGroup) {
    // Go through all the check boxes. return an array of all the ones
    // that are selected (their position numbers). if no boxes were checked,
    // returned array will be empty (length will be zero)
    if (buttonGroup == null)
        return 0;
    var count = 0;
    if (buttonGroup[0]) { // if the button group is an array (one check box is not an array)
        for (var i=0; i<buttonGroup.length; i++) {
            if (buttonGroup[i].checked) {
                count ++;
            }
        }
    } else { // There is only one check box (it's not an array)
        if (buttonGroup.checked) { // if the one check box is checked
            count ++;
        }
    }
    return count;
} // Ends the "getSelectedCheckbox" function

function doAction(form, currentObj, lstSelectionAsk, objChecks) {

    if (getCountSelectedCheckbox(objChecks) == 0) {
        currentObj.options.selectedIndex = 0;
        return;
    }

    var ask = false;
    if (lstSelectionAsk[0]) {
        for(i = 0; i < lstSelectionAsk.length; i++) {
            if (lstSelectionAsk[i] == currentObj.options.selectedIndex) {
                ask = true;
                break;
            }
        }
    } else {
        if (lstSelectionAsk == currentObj.options.selectedIndex)
            ask = true;
    }

    if (ask) {
        var result = confirm('Are you sure?');
        if (result == true)
            form.submit();
        else
            currentObj.options.selectedIndex = 0;
    } else {
        form.submit();
    }
}

function togglePublishType() {
    if (document.getElementById('publishtype').innerHTML != 'at') {
        document.getElementById('publishtype').innerHTML = 'at';
        document.getElementById('immediately').value = '0';
    } else {
        document.getElementById('publishtype').innerHTML = 'immediately';
        document.getElementById('immediately').value = '1';
    }
    $('#publishat').slideToggle('fast');
}

function togglePublishedType() {
    if (document.getElementById("editpublishedat").innerHTML == '[edit]') {
        document.getElementById("editpublishedat").innerHTML = '[cancel]';
        document.getElementById('immediately').value = '0';
    } else {
        document.getElementById("editpublishedat").innerHTML = '[edit]';
        document.getElementById('immediately').value = '1';
    }
    $('#publishat').slideToggle('fast');
}

function toggleNewHotPost() {
    if (document.getElementById("editHot").value == '0') {
        document.getElementById("editHot").value = '1';
    } else {
        document.getElementById("editHot").value = '0';
    }
    $('#hotat').slideToggle('fast');
}


function toggleEditHotPost() {
    if (document.getElementById("hotpost")) {
        if (document.getElementById("hotpost").innerHTML == '[edit]') {
            document.getElementById("hotpost").innerHTML = '[cancel]';
            document.getElementById("editHot").value = '1';
        } else {
            document.getElementById("hotpost").innerHTML = '[edit]';
            document.getElementById("editHot").value = '0';
        }
    }
    $('#hotat').slideToggle('fast');
}

function getSelectedCheckbox(buttonGroup) {
    // Go through all the check boxes. return an array of all the ones
    // that are selected (their position numbers). if no boxes were checked,
    // returned array will be empty (length will be zero)
    if (buttonGroup == null)
        return -1;
    var retArr = new Array();
    var lastElement = 0;
    if (buttonGroup[0]) { // if the button group is an array (one check box is not an array)
        for (var i=0; i<buttonGroup.length; i++) {
            if (buttonGroup[i].checked) {
                retArr.length = lastElement;
                retArr[lastElement] = i;
                lastElement++;
            }
        }
    } else { // There is only one check box (it's not an array)
        if (buttonGroup.checked) { // if the one check box is checked
            retArr.length = lastElement;
            retArr[lastElement] = 0; // return zero as the only array value
        }
    }
    return retArr;
} // Ends the "getSelectedCheckbox" function

function getSelectedCheckboxValue(buttonGroup) {
    // return an array of values selected in the check box group. if no boxes
    // were checked, returned array will be empty (length will be zero)
    if (buttonGroup == null)
        return null;
    var retArr = new Array(); // set up empty array for the return values
    var selectedItems = getSelectedCheckbox(buttonGroup);
    if (selectedItems.length != 0) { // if there was something selected
        retArr.length = selectedItems.length;
        for (var i=0; i<selectedItems.length; i++) {
            if (buttonGroup[selectedItems[i]]) { // Make sure it's an array
                retArr[i] = buttonGroup[selectedItems[i]].value;
            } else { // It's not an array (there's just one check box and it's selected)
                retArr[i] = buttonGroup.value;// return that value
            }
        }
    }
    return retArr;
} // Ends the "getSelectedCheckBoxValue" function

//Select user
var linkEditUser;
function selectUsers(strObjReturn, linkEdit) {
    linkEditUser = linkEdit;
    var parent = window.opener.document;
    var values = getSelectedCheckboxValue(document.users.userids);
    var span = parent.getElementById(strObjReturn);
    var hidden = parent.getElementById(strObjReturn + "hidden");
    for (var i = 0; i < values.length; i++) {
        if (!values[i])
            continue;
        addUser(hidden, values[i], document.getElementById("username" + values[i]).innerHTML);
    }
    validateUserSpan(hidden, span);
    window.close();
    parent.focus();
}

function addUser(hidden, userid, username) {
    var lstUser = hidden.value.split(';');
    var node;
    for (var i = 0; i < lstUser.length; i++) {
        node = lstUser[i].split(',');
        if (node[0] == userid)
            return;
    }
    node = userid + ',' + username;
    if (hidden.value.trim() == "")
        hidden.value = node;
    else
        hidden.value += ';' + node;
}

function removeUser(strHidden, strSpan, userid) {
    hidden = document.getElementById(strHidden);
    span = document.getElementById(strSpan);
    var lstUser = hidden.value.split(';');
    var node;
    hidden.value = "";
    for (var i = 0; i < lstUser.length; i++) {
        node = lstUser[i].split(',');
        if (node[0] != userid) {
            if (hidden.value.trim() == "")
                hidden.value = lstUser[i];
            else
                hidden.value += ';' + lstUser[i];
        }
    }

    validateUserSpan(hidden, span);
}

function validateUserSpan(hidden, span) {
    
    var lstUser = hidden.value.split(';');
    var node;
    span.innerHTML = "";
    if (hidden.value != "") {
        for (var i = 0; i < lstUser.length; i++) {
            node = lstUser[i].split(',');
            var element = '<a href="' + linkEditUser + '?uid=' + node[0]
            + '" target="_edituser">' + node[1]
            + '</a> <span style="font-size:10px">[<a href="javascript:" onclick="removeUser(\'' + hidden.id + '\',\'' + span.id + '\',\'' + node[0] + '\')">X</a>]</span>';
            if (span.innerHTML.trim() == "") {
                span.innerHTML = element;
            } else {
                span.innerHTML += ', ' + element;
            }
        }
    }
}
//End select user

//Select posts
var linkEditPost;
function selectPosts(strObjReturn, linkEdit) {
    linkEditPost = linkEdit;
    var parent = window.opener.document;
    var values = getSelectedCheckboxValue(document.posts.postids);
    var span = parent.getElementById(strObjReturn);
    var hidden = parent.getElementById(strObjReturn + "hidden");

    for (var i = 0; i < values.length; i++) {
        if (!values[i])
            continue;
        
        if (parent.getElementById('postid') && parent.getElementById('postid').value == values[i])
            continue;
        addPost(hidden, values[i], document.getElementById("title" + values[i]).innerHTML);
    }
    validatePostSpan(hidden, span);
    window.close();
    parent.focus();
}

function addPost(hidden, postid, title) {
    var lstPost = hidden.value.split(';');
    var node;
    for (var i = 0; i < lstPost.length; i++) {
        node = lstPost[i].split('|');
        if (node[0] == postid)
            return;
    }
    node = postid + '|' + title;
    if (hidden.value.trim() == "")
        hidden.value = node;
    else
        hidden.value += ';' + node;
}

function removePost(strHidden, strSpan, postid) {
    hidden = document.getElementById(strHidden);
    span = document.getElementById(strSpan);
    var lstPost = hidden.value.split(';');
    var node;
    hidden.value = "";
    for (var i = 0; i < lstPost.length; i++) {
        node = lstPost[i].split('|');
        if (node[0] != postid) {
            if (hidden.value.trim() == "")
                hidden.value = lstPost[i];
            else
                hidden.value += ';' + lstPost[i];
        }
    }

    validatePostSpan(hidden, span);
}

function validatePostSpan(hidden, span) {

    var lstPost = hidden.value.split(';');
    var node;
    span.innerHTML = "";
    if (hidden.value != "") {
        for (var i = 0; i < lstPost.length; i++) {
            node = lstPost[i].split('|');
            var view = '<a href="' + linkEditPost + '?pid=' + node[0] + '" target="_editpost">View</a>';
            var remove = '<a href="javascript:" onclick="removePost(\'' + hidden.id + '\',\'' + span.id + '\',\'' + node[0] + '\')">Remove</a>';
            var element = '<li><b>' + node[1] + '</b> <br/>' + view + ' | ' + remove + '</li>';
            span.innerHTML += element;
        }
    }
}
//End select post

function checkFileExtension(elem) {
    var filePath = elem.value;

    if(filePath.indexOf('.') == -1)
        return false;

    var validExtensions = new Array();
    var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();

    validExtensions[0] = 'jpg';
    validExtensions[1] = 'jpeg';
    validExtensions[2] = 'bmp';
    validExtensions[3] = 'png';
    validExtensions[4] = 'gif';

    for(var i = 0; i < validExtensions.length; i++) {
        if(ext == validExtensions[i])
            return true;
    }

    alert('The file extension ' + ext.toUpperCase() + ' is not allowed!');
    return false;
}