function checkPostComment() {
    if (document.getElementById("comment_author").value == "") {
        alert('Vui lòng nhập tên người gửi');
        document.getElementById("comment_author").focus();
        return false;
    }
    if (document.getElementById("comment_content").value == "") {
        alert('Vui lòng nhập nội dung cần gửi');
        document.getElementById("comment_content").focus();
        return false;
    }
    if (document.getElementById("scode").value == "") {
        alert('Vui lòng nhập scode');
        document.getElementById("scode").focus();
        return false;
    }
    return true;
}

function checkAddSub() {
    if (isNaN(document.getElementById("sub_priceperday").value)) {
        alert('Price Per Day is invalid!');
        document.getElementById("sub_priceperday").focus();
        return false;
    }
    return true;
}

function checkAddPost() {

    if (document.getElementById("immediately").value == "0") {
        if (document.getElementsByName("dojo.publisheddate")[0].value == '') {
            alert('Publish date is invalid!');
            document.getElementsByName("dojo.publisheddate")[0].focus();
            return false;
        }
        
        if (document.getElementById("hourPublish").value == ''
            || isNaN(document.getElementById("hourPublish").value)
            || parseInt(document.getElementById("hourPublish").value, 10) < 0
            || parseInt(document.getElementById("hourPublish").value, 10) > 23) {
            alert('Publish date is invalid!');
            document.getElementById("hourPublish").focus();
            return false;
        }

        if (document.getElementById("minutePublish").value == ''
            || isNaN(document.getElementById("minutePublish").value)
            || parseInt(document.getElementById("minutePublish").value, 10) < 0
            || parseInt(document.getElementById("minutePublish").value, 10) > 59) {
            alert('Publish date is invalid!');
            document.getElementById("minutePublish").focus();
            return false;
        }
    }

    if (document.getElementById("editHot").value == '1') {
        if (document.getElementsByName("dojo.dateHot")[0].value == '') {
            alert('Hot date is invalid!');
            document.getElementsByName("dojo.dateHot")[0].focus();
            return false;
        }

        if (document.getElementById("hourHot").value == ''
            || isNaN(document.getElementById("hourHot").value)
            || parseInt(document.getElementById("hourHot").value, 10) < 0
            || parseInt(document.getElementById("hourHot").value, 10) > 23) {
            alert('Hot date is invalid!');
            document.getElementById("hourHot").focus();
            return false;
        }

        if (document.getElementById("minuteHot").value == ''
            || isNaN(document.getElementById("minuteHot").value)
            || parseInt(document.getElementById("minuteHot").value, 10) < 0
            || parseInt(document.getElementById("minuteHot").value, 10) > 59) {
            alert('Hot date is invalid!');
            document.getElementById("minuteHot").focus();
            return false;
        }
    }
    return true;
}