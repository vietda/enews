/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vietdiep.enews.utils;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author vietda
 */
public class DateTimeUtil {

    public static String getStringDateFormatForPost(long milis) {
        Date date = new Date(milis);
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        DateFormat dateFormat1 =  new SimpleDateFormat("EEEEE");
        String viDayOfWeek = dateFormat1.format(date);
        switch(viDayOfWeek){
            case "Monday":
                viDayOfWeek = "Thứ Hai, ngày ";
                break;
            case "Tuesday":
                viDayOfWeek = "Thứ Ba, ngày ";
                break;
            case "Wednesday":
                viDayOfWeek = "Thứ Tư, ngày ";
                break;
            case "Thursday":
                viDayOfWeek = "Thứ Năm, ngày ";
                break;
            case "Friday":
                viDayOfWeek = "Thứ Sáu, ngày ";
                break;
            case "Saturday":
                viDayOfWeek = "Thứ Bảy, ngày ";
                break;
            case "Sunday":
                viDayOfWeek = "Chủ Nhật, ngày ";
                break;
            default:
                viDayOfWeek = "";
                break;
        }
            
        return String.format("%s%s", viDayOfWeek, dateFormat.format(date));
    }
    
    public static String getStringDateFormat(long milis) {
        Date date = new Date(milis);
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return dateFormat.format(date);
    }
    
//    public static void main(String[] args){
//        String date = getStringDateFormat(System.currentTimeMillis());
//        System.out.println(date);
//    }
}
