/*
 * Copyright 2017 Home Ltd.
 */
package com.vietdiep.enews.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.vietdiep.enews.model.Post;
import java.util.List;
import java.util.Map;
import com.vietdiep.enews.bussiness.PostBusiness;
import com.vietdiep.enews.model.PostViewer;
/**
 *
 * @author Viet Diep
 */
public class PublishedByCategoryAction extends ActionSupport {

    private PostBusiness postService = new PostBusiness();
    private List<Post> postsPublishedByCategory;
    List<PostViewer> postsPublishedByCategoryView;

    @Override
    public String execute() {
        Map m = ActionContext.getContext().getParameters();
        String value = m.get("cid").toString();
        int categoryId = Integer.parseInt(value);
        postsPublishedByCategory = postService.getPostsPublishedByCategory(categoryId);
        postsPublishedByCategoryView = PostViewer.instance.getListPostViewer(postsPublishedByCategory);
        return SUCCESS;
    }

    public List<Post> getPostsPublishedByCategory() {
        return postsPublishedByCategory;
    }

    public void setPostsPublishedByCategory(List<Post> postsPublishedByCategory) {
        this.postsPublishedByCategory = postsPublishedByCategory;
    }

    public PostBusiness getPostService() {
        return postService;
    }

    public void setPostService(PostBusiness postService) {
        this.postService = postService;
    }

    public List<PostViewer> getPostsPublishedByCategoryView() {
        return postsPublishedByCategoryView;
    }

    public void setPostsPublishedByCategoryView(List<PostViewer> postsPublishedByCategoryView) {
        this.postsPublishedByCategoryView = postsPublishedByCategoryView;
    }
    
    
}
