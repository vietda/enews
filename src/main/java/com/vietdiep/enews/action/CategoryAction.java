/*
 * Copyright 2017 Home Ltd.
 */
package com.vietdiep.enews.action;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.vietdiep.enews.bussiness.CategoryBusiness;
import com.vietdiep.enews.bussiness.PostBusiness;
import com.vietdiep.enews.model.Advertisement;
import com.vietdiep.enews.model.Category;
import com.vietdiep.enews.model.CategoryNameAndParentCategoryName;
import com.vietdiep.enews.model.Post;
import com.vietdiep.enews.model.PostViewer;
import java.util.List;
import java.util.Map;
/**
 *
 * @author Viet Diep
 */
public class CategoryAction extends ActionSupport{
    
    PostBusiness postService = new PostBusiness();
    CategoryBusiness categoryService = new CategoryBusiness();
    List<Post> posts;
    CategoryBusiness category = new CategoryBusiness();
    List<Category> categories;
    List<Post> otherPosts;
    List<Advertisement> advertisements;
    
    List<PostViewer> postsView;
    List<PostViewer> otherPostsView;
    
    @Override
    public String execute(){
        
        Map m = ActionContext.getContext().getParameters();
        String value = m.get("categoryid").toString();
        int categoryId = Integer.parseInt(value);
        //get data for header menu
        categories = categoryService.getCategories();
        //get list post
        posts = postService.getPostsPublishedByCategory(categoryId);
        postsView = PostViewer.instance.getListPostViewer(posts);
        //get other post
        otherPosts = postService.getLastedPublishedHightlightPosts();
        otherPostsView = PostViewer.instance.getListPostViewer(otherPosts);
        return SUCCESS;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<Post> getOtherPosts() {
        return otherPosts;
    }

    public void setOtherPosts(List<Post> otherPosts) {
        this.otherPosts = otherPosts;
    }

    public List<Advertisement> getAdvertisements() {
        return advertisements;
    }

    public void setAdvertisements(List<Advertisement> advertisements) {
        this.advertisements = advertisements;
    }

    public PostBusiness getPostService() {
        return postService;
    }

    public void setPostService(PostBusiness postService) {
        this.postService = postService;
    }

    public CategoryBusiness getCategoryService() {
        return categoryService;
    }

    public void setCategoryService(CategoryBusiness categoryService) {
        this.categoryService = categoryService;
    }

    public CategoryBusiness getCategory() {
        return category;
    }

    public void setCategory(CategoryBusiness category) {
        this.category = category;
    }

    public List<PostViewer> getPostsView() {
        return postsView;
    }

    public void setPostsView(List<PostViewer> postsView) {
        this.postsView = postsView;
    }

    public List<PostViewer> getOtherPostsView() {
        return otherPostsView;
    }

    public void setOtherPostsView(List<PostViewer> otherPostsView) {
        this.otherPostsView = otherPostsView;
    }
    
}