/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vietdiep.enews.action;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.vietdiep.enews.model.Comment;
import java.util.Map;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.interceptor.ServletRequestAware;

/**
 *
 * @author vietda
 */
public class PostCommentAction extends ActionSupport implements ServletRequestAware{
    
    Comment comment;
    HttpServletRequest request;
    
    @Override
    public String execute() {
        this.comment.setCreatedTime(System.currentTimeMillis());
        return SUCCESS;
    }
    
    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    @Override
    public void setServletRequest(HttpServletRequest req) {
        this.request = req;
    }
    
}
