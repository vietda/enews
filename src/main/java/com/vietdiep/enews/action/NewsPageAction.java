/*
 * Copyright 2017 Home Ltd.
 */
package com.vietdiep.enews.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.vietdiep.enews.bussiness.AdvertisementBussiness;
import com.vietdiep.enews.bussiness.CategoryBusiness;
import com.vietdiep.enews.bussiness.CommentBusiness;
import com.vietdiep.enews.bussiness.PostBusiness;
import com.vietdiep.enews.model.Advertisement;
import com.vietdiep.enews.model.Category;
import com.vietdiep.enews.model.CategoryNameAndParentCategoryName;
import com.vietdiep.enews.model.Post;
import com.vietdiep.enews.model.Comment;
import com.vietdiep.enews.model.CommentViewer;
import com.vietdiep.enews.model.PostViewer;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author Viet Diep
 */
public class NewsPageAction extends ActionSupport {

    CategoryBusiness categoryService = new CategoryBusiness();
    PostBusiness postService = new PostBusiness();
    CommentBusiness commentServicce = new CommentBusiness();
    private List<Category> categories;
    AdvertisementBussiness adsService = new AdvertisementBussiness();
    List<Advertisement> advertisements;
    List<Post> otherPosts;
    Post post;
    List<CommentViewer> commentsViewer;
    Comment comment;
    List<PostViewer> otherPostsView;
    PostViewer postView;
    static boolean isFirstVisit = true;
    static int currentPostId = 0;
    String commentAuthor = "";
    String commentContent = "";
    String inputCode = "";
    String scode = "";
    boolean isValidComment = true;

    @Override
    public String execute() {

        Map m = ActionContext.getContext().getParameters();
        String value = m.get("postid").toString();
        if(!value.equalsIgnoreCase("Empty{name='postid'}")){
            currentPostId = Integer.parseInt(value);
        } 
        if (isValidComment && !isFirstVisit) {
            CommentBusiness.addCommentToPost(commentAuthor, commentContent, currentPostId);
        }
        List<Comment> comments = commentServicce.getCommentsByPostId(currentPostId);
        commentsViewer = CommentViewer.instance.getListCommentViewer(comments);
        post = postService.getPost(currentPostId);
        postView = PostViewer.instance.getPostViewer(post);
        int categoryIDOfPost = post.getCategoryID();
        categories = categoryService.getCategories();
        otherPosts = postService.getLastedPublishedHightlightPosts();
        otherPostsView = PostViewer.instance.getListPostViewer(otherPosts);
        isFirstVisit = false;
        return SUCCESS;
    }

    @Override
    public void validate() {
        if (isFirstVisit) {
            return;
        }
        if (commentAuthor.equalsIgnoreCase("")) {
            isValidComment = false;
        }
        if (commentContent.equalsIgnoreCase("")) {
            isValidComment = false;
        }
        if (!inputCode.equalsIgnoreCase(scode)) {
            isValidComment = false;
        }
    }

    public boolean isIsFirstVisit() {
        return isFirstVisit;
    }

    public void setIsFirstVisit(boolean isFirstVisit) {
        isFirstVisit = isFirstVisit;
    }

    public int getCurrentPostId() {
        return currentPostId;
    }

    public static void setCurrentPostId(int currentPostId) {
        currentPostId = currentPostId;
    }

    public String getCommentAuthor() {
        return commentAuthor;
    }

    public void setCommentAuthor(String commentAuthor) {
        this.commentAuthor = commentAuthor;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public String getScode() {
        return scode;
    }

    public void setScode(String scode) {
        this.scode = scode;
    }

    public CategoryBusiness getCategoryService() {
        return categoryService;
    }

    public void setCategoryService(CategoryBusiness categoryService) {
        this.categoryService = categoryService;
    }

    public PostBusiness getPostService() {
        return postService;
    }

    public void setPostService(PostBusiness postService) {
        this.postService = postService;
    }

    public CommentBusiness getCommentServicce() {
        return commentServicce;
    }

    public void setCommentServicce(CommentBusiness commentServicce) {
        this.commentServicce = commentServicce;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public AdvertisementBussiness getAdsService() {
        return adsService;
    }

    public void setAdsService(AdvertisementBussiness adsService) {
        this.adsService = adsService;
    }

    public List<Advertisement> getAdvertisements() {
        return advertisements;
    }

    public void setAdvertisements(List<Advertisement> advertisements) {
        this.advertisements = advertisements;
    }

    public List<Post> getOtherPosts() {
        return otherPosts;
    }

    public void setOtherPosts(List<Post> otherPosts) {
        this.otherPosts = otherPosts;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public List<PostViewer> getOtherPostsView() {
        return otherPostsView;
    }

    public void setOtherPostsView(List<PostViewer> otherPostsView) {
        this.otherPostsView = otherPostsView;
    }

    public PostViewer getPostView() {
        return postView;
    }

    public void setPostView(PostViewer postView) {
        this.postView = postView;
    }

    public List<CommentViewer> getCommentsViewer() {
        return commentsViewer;
    }

    public void setCommentsViewer(List<CommentViewer> commentsViewer) {
        this.commentsViewer = commentsViewer;
    }

}
