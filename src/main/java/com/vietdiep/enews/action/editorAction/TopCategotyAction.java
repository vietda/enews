/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vietdiep.enews.action.editorAction;

import com.opensymphony.xwork2.ActionSupport;
import com.vietdiep.enews.bussiness.CategoryBusiness;
import com.vietdiep.enews.bussiness.PostBusiness;
import com.vietdiep.enews.bussiness.UserBusiness;
import com.vietdiep.enews.model.Post;
import com.vietdiep.enews.model.PostViewer;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.interceptor.ServletRequestAware;

/**
 *
 * @author vietda
 */
public class TopCategotyAction extends ActionSupport{
    
    static String requestMeta = "";
    HttpServletRequest request;
            
    @Override
    public String execute(){
        
        requestMeta = "topcategory";
        return SUCCESS;
    }

    public static String getRequestMeta() {
        return requestMeta;
    }

    public static void setRequestMeta(String requestMeta) {
        TopCategotyAction.requestMeta = requestMeta;
    }

}
