/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vietdiep.enews.bussiness;

import com.vietdiep.enews.database.DBConnection;
import com.vietdiep.enews.model.Category;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
/**
 *
 * @author vietda
 */
public class CategoryBusiness {
    
    private static CategoryBusiness instance = new CategoryBusiness();
    
    public static CategoryBusiness getInstance(){
        return instance;
    }
    
    public List<Category> getCategories(){
        
        List<Category> categories = new ArrayList<Category>();
        Connection connection = DBConnection.getInstance().getConnection();
        if(connection!=null){
            String query = "select categoryID,parentID,name,positionNum from CATEGORY";
            try {
                PreparedStatement statement = connection.prepareStatement(query);
                ResultSet resultSet = statement.executeQuery();
                while(resultSet.next()){
                    Category category = new Category();
                    category.setCategoryID(resultSet.getInt("categoryID"));
                    category.setParentID(resultSet.getInt("parentID"));
                    category.setPositionNumber(resultSet.getInt("positionNum"));
                    category.setCategoryName(resultSet.getString("name"));
                    categories.add(category);
                }
            } catch (SQLException ex) {
                Logger.getLogger(CategoryBusiness.class.getName()).log(Level.SEVERE, null, ex);
            } finally{
                DBConnection.getInstance().returnConnectionToPool(connection);
            }
            
        }
        List<Category> rootCategories = categories.stream().filter(c -> c.getParentID() == 0).collect(Collectors.toList());
        for (Category rootCategory : rootCategories) {
            List<Category> childCategories = new ArrayList<>();
            for (Category cat : categories) {
                if (cat.getParentID() == rootCategory.getCategoryID()) {
                    childCategories.add(cat);
                }
            }
            rootCategory.setChildCategories(childCategories);
        }
        return rootCategories;
    }
    
    public List<Category> getListAllCategory() {
        Connection connection = DBConnection.getInstance().getConnection();
        List<Category> categories = null;
        if (connection != null) {
            try {
                String string = "select * from CATEGORY";
                PreparedStatement statement = connection.prepareStatement(string);
                ResultSet rs = statement.executeQuery();
                categories = new ArrayList<>();
                while (rs.next()) {
                    Category category = new Category();
                    category.setCategoryID(rs.getInt("categoryID"));
                    category.setCategoryName(rs.getString("name"));
                    category.setCreatedDtm(rs.getLong("createdDtm"));
                    category.setParentID(rs.getInt("parentID"));
                    category.setPositionNumber(rs.getInt("positionNum"));
                    category.setTotalView(rs.getInt("totalView"));
                    categories.add(category);
                }
            } catch (SQLException ex) {
                Logger.getLogger(CategoryBusiness.class.getName()).log(Level.SEVERE, null, ex);
            } finally{
                DBConnection.getInstance().returnConnectionToPool(connection);
            }
        }
        return categories;
    }
    
    public String getCategoryNameById(int id){
        Connection connection = DBConnection.getInstance().getConnection();
        List<Category> categories = null;
        String name = "";
        if (connection != null) {
            try {
                String string = "select name from CATEGORY where categiryID = ?";
                PreparedStatement statement = connection.prepareStatement(string);
                statement.setInt(1, id);
                ResultSet rs = statement.executeQuery();
                categories = new ArrayList<>();
                while (rs.next()) {
                    name = rs.getString("name");
                    return name;
                }
            } catch (SQLException ex) {
                Logger.getLogger(CategoryBusiness.class.getName()).log(Level.SEVERE, null, ex);
            } finally{
                DBConnection.getInstance().returnConnectionToPool(connection);
            }
        }
        return "";
    }
}
