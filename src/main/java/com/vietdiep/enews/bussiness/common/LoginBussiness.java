/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vietdiep.enews.bussiness.common;

import com.vietdiep.enews.database.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.digest.DigestUtils;
import static org.apache.commons.codec.digest.DigestUtils.md5Hex;
import static org.apache.commons.codec.digest.DigestUtils.md5Hex;
import static org.apache.commons.codec.digest.DigestUtils.md5Hex;

/**
 *
 * @author vietda
 */
public class LoginBussiness {

    //MD5 hash
    public static String hashGivenPassword(String givenPassword) {
        String hash = "ZXCVBNMASDFGHJKLPOIUYTREWQzxcvbnmlkjhgfdsaqwertyuiop123456789";
        String passwordHash = DigestUtils.md5Hex(givenPassword).toUpperCase();
        System.out.println(passwordHash);
        return passwordHash;
    }
    
    public static boolean isValidInput(String fieldName, String value){
        Connection connection = DBConnection.getInstance().getConnection();
        if(connection == null){
            return false;
        }
        String string = "select userID from USERS where userName = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(string);
            statement.setString(1, value);
            ResultSet rs = statement.executeQuery();
            while(!rs.next()){
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(LoginBussiness.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.getInstance().returnConnectionToPool(connection);
        }
        return true;
    }
    
    public static int checkLogin(String userName, String hashPassword){
        Connection connection = DBConnection.getInstance().getConnection();
        int userID = 0;
        if(connection == null){
            return 0;
        }
        String string = "select userID from USERS where userName = ? and password = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(string);
            statement.setString(1, userName);
            statement.setString(2, hashPassword);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                userID = rs.getInt("userID");
            }
        } catch (SQLException ex) {
            Logger.getLogger(LoginBussiness.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.getInstance().returnConnectionToPool(connection);
        }
        return userID;
    }
    
    public static void main(String[] args){
        String  hashPass = hashGivenPassword("123456");
    }
    
    
    
}
