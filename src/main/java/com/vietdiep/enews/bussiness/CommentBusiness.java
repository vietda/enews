/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vietdiep.enews.bussiness;

import com.mysql.cj.protocol.Resultset;
import com.vietdiep.enews.database.DBConnection;
import com.vietdiep.enews.model.Comment;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vietda
 */
public class CommentBusiness {

    public List<Comment> getCommentsByPostId(int postId) {
        Connection connection = DBConnection.getInstance().getConnection();
        List<Comment> comments = null;
        if (connection != null) {
            comments = new ArrayList<>();
            String string = "SELECT * FROM COMMENT WHERE postid = ? AND status = 'C_Published'";
            try {
                PreparedStatement statement = connection.prepareStatement(string);
                statement.setInt(1, postId);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    Comment comment = new Comment();
                    comment.setCommentId(rs.getInt("id"));
                    comment.setCreatedTime(rs.getLong("createdtime"));
                    comment.setContent(rs.getString("content"));
                    comment.setPosiId(rs.getInt("postid"));
                    comment.setAuthorName(rs.getString("authorname"));
                    comments.add(comment);
                }
            } catch (SQLException ex) {
                Logger.getLogger(CommentBusiness.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return comments;
    }

    public static int addCommentToPost(String authorName, String content, int postId) {
        Connection connection = DBConnection.getInstance().getConnection();
        if (connection != null) {
            String string = "INSERT INTO COMMENT(content,authorname,postid,createdtime,status) values (?,?,?,?,?)";
            try {
                PreparedStatement statement = connection.prepareStatement(string);
                statement.setString(1, content);
                statement.setString(2, authorName);
                statement.setInt(3, postId);
                statement.setLong(4, System.currentTimeMillis());
                statement.setString(5, "C_Pending");
                int rs = statement.executeUpdate();
                if(rs != 0){
                    return rs;
                }
            } catch (SQLException ex) {
                Logger.getLogger(CommentBusiness.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }
    
}
