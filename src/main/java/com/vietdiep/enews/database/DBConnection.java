/*
 * Copyright 2017 Home Ltd.
 */
package com.vietdiep.enews.database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Viet Diep
 */

public class DBConnection {
    
    private static final int _poolSize = 10;
    private static final String jdbcDriver = "com.mysql.jdbc.Driver";
    private static final String userName = "root";
    private static final String password = "root";
    private static final String url = "jdbc:mysql://localhost:3306/enews";
    
    private static DBConnection instance;
    private static Queue<Connection> _pool = new LinkedList<>();
    
    public static DBConnection getInstance(){
        if(instance == null){
            instance = new DBConnection();
            initializePool();
        }
        return instance;
    }
            
    private DBConnection(){}
            
    public static void initializePool(){
        for(int i = 0; i < _poolSize; i++){
            try {
                Class.forName(jdbcDriver);
                Connection newConnection = DriverManager.getConnection(url,userName,password);
                _pool.add(newConnection);
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public Connection getConnection(){
        return _pool.poll();
    }
    
    public void returnConnectionToPool(Connection connection){
        _pool.add(connection);
    }
}
