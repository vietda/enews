/*
 * Copyright 2017 Home Ltd.
 */
package com.vietdiep.enews.backend.action.adminAction;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import com.vietdiep.enews.model.Category;
import com.vietdiep.enews.model.KeyValue;
import com.vietdiep.enews.model.User;
import com.vietdiep.enews.bussiness.CategoryBusiness;
import com.vietdiep.enews.bussiness.UserBusiness;
import com.vietdiep.enews.bussiness.common.LoginBussiness;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.interceptor.ServletRequestAware;

/**
 *
 * @author Viet Diep
 */
public class AddUserAction extends ActionSupport implements ServletRequestAware{

    HttpServletRequest request;
    User user;
    private List<String> listPermission = Arrays.asList(
            new String[]{"", "Admin", "Editor", "Reporter", "Comment Approver",});
    List<Category> categories;
    UserBusiness userBusiness = new UserBusiness();
    CategoryBusiness categoryBusiness = new CategoryBusiness();
    String username;
    String password;
    String passwordAgain;
    String email;
    String emailAgain;
    String permissionChoose;
    String fullName;
    String address;
    String phoneNumber;
    int status;
    String requestMeta = "";
    boolean isFirstVisit = true;

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.request = hsr;
    }
    
    @Override
    public String execute() {
        //this.request.setAttribute("actionName", "addUser");
        this.requestMeta = "addUser";
        if (!isFirstVisit) {
            User user = new User();
            user.setAddress(this.address);
            user.setCreateDTM(System.currentTimeMillis());
            user.setEmail(this.email);
            user.setFullName(this.fullName);
            user.setIsLock(0);
            user.setLastUpdateTime(System.currentTimeMillis());
            user.setPassWord(LoginBussiness.hashGivenPassword(this.password));
            user.setPhoneNumber(this.phoneNumber);
            user.setUserName(this.username);
            user.setRole(this.permissionChoose);
            UserBusiness.instance.insertUser(user);
        }
        isFirstVisit = false;
        return INPUT;
    }

    @Override
    public void validate() {
        this.requestMeta = "addUser";
        if (isFirstVisit) {
            return;
        }
        if (this.username.equals("")) {
            addFieldError("invUsername", "Username is required");
        } 
        if(this.password.equalsIgnoreCase("")){
            addFieldError("invPassword", "Password is requied");
        }
        if(!this.passwordAgain.equalsIgnoreCase(password)){
            addFieldError("invPassword", "Password is wrong");
        }
        if (this.email.equalsIgnoreCase("")) {
            addFieldError("invEmail", "Email is required");
        }
        if (!this.email.equalsIgnoreCase(this.emailAgain)) {
            addFieldError("invEmail", "Email Again is wrong");
        }
        if (this.permissionChoose.equals("")) {
            addFieldError("invPermission", "Permission is required");
        }
        if (this.fullName.equalsIgnoreCase("")) {
            addFieldError("invFullname", "Fullname is required");
        }
        if (this.address.equalsIgnoreCase("")) {
            addFieldError("invAddress", "Address is required");
        }
        if (this.phoneNumber.equalsIgnoreCase("")) {
            addFieldError("invPhonenumber", "Phone Number is required");
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<String> getListPermission() {
        return listPermission;
    }

    public void setListPermission(List<String> listPermission) {
        this.listPermission = listPermission;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public UserBusiness getUserBusiness() {
        return userBusiness;
    }

    public void setUserBusiness(UserBusiness userBusiness) {
        this.userBusiness = userBusiness;
    }

    public CategoryBusiness getCategoryBusiness() {
        return categoryBusiness;
    }

    public void setCategoryBusiness(CategoryBusiness categoryBusiness) {
        this.categoryBusiness = categoryBusiness;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordAgain() {
        return passwordAgain;
    }

    public void setPasswordAgain(String passwordAgain) {
        this.passwordAgain = passwordAgain;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailAgain() {
        return emailAgain;
    }

    public void setEmailAgain(String emailAgain) {
        this.emailAgain = emailAgain;
    }

    public String getPermissionChoose() {
        return permissionChoose;
    }

    public void setPermissionChoose(String permissionChoose) {
        this.permissionChoose = permissionChoose;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean getIsFirstVisit() {
        return isFirstVisit;
    }

    public void setIsFirstVisit(boolean isFirstVisit) {
        this.isFirstVisit = isFirstVisit;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public String getRequestMeta() {
        return requestMeta;
    }

    public void setRequestMeta(String requestMeta) {
        this.requestMeta = requestMeta;
    }

}
