/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vietdiep.enews.backend.action.adminAction;

import static com.opensymphony.xwork2.Action.INPUT;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.interceptor.ServletRequestAware;

/**
 *
 * @author vietda
 */
public class AddCategoryAction extends ActionSupport{

    HttpServletRequest request;
    static String requestMeta = "";
    
    @Override
    public String execute() throws Exception {
        requestMeta = "addUser";
        return INPUT;
    }        

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public static String getRequestMeta() {
        return requestMeta;
    }

    public static void setRequestMeta(String requestMeta) {
        AddCategoryAction.requestMeta = requestMeta;
    }
    
}
