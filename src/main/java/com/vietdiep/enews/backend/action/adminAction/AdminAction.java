/*
 * Copyright 2017 Home Ltd.
 */
package com.vietdiep.enews.backend.action.adminAction;

import com.opensymphony.xwork2.ActionSupport;
import com.vietdiep.enews.model.KeyValue;
import com.vietdiep.enews.model.User;
import com.vietdiep.enews.bussiness.UserBusiness;
import com.vietdiep.enews.model.UserViewer;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.interceptor.ServletRequestAware;

/**
 *
 * @author Viet Diep
 */
public class AdminAction extends ActionSupport implements ServletRequestAware {

    private HttpServletRequest request;
    private String keyword;
    private List<KeyValue> listSortBy = Arrays.asList(
            new KeyValue[]{new KeyValue("createdDate", "Created Date"), new KeyValue("username", "Username")});
    private List<KeyValue> listOrderBy = Arrays.asList(
            new KeyValue[]{new KeyValue("desc", "Descending"), new KeyValue("asc", "Ascending")});
    private List<String> listPermission = Arrays.asList(
            new String[]{"All Permission", "Admin", "Editor", "Reporter", "Comment Approver", "Ads Manager"});
    String[] action; 
    String sortBy;
    String orderBy;
    String filterPermission;
    List<User> users;
    List<UserViewer> userViewer;
    UserBusiness userService = new UserBusiness();
    String requestMeta = "";
            
    @Override
    public String execute(){
        
        requestMeta = "homepage";
        users = userService.getListUser();
        userViewer = UserViewer.instance.getUserViewer(users);
        return SUCCESS;
    }

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.request = hsr;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public List<KeyValue> getListSortBy() {
        return listSortBy;
    }

    public void setListSortBy(List<KeyValue> listSortBy) {
        this.listSortBy = listSortBy;
    }

    public List<KeyValue> getListOrderBy() {
        return listOrderBy;
    }

    public void setListOrderBy(List<KeyValue> listOrderBy) {
        this.listOrderBy = listOrderBy;
    }

    public UserBusiness getUserService() {
        return userService;
    }

    public void setUserService(UserBusiness userService) {
        this.userService = userService;
    }

    public List<String> getListPermission() {
        return listPermission;
    }

    public void setListPermission(List<String> listPermission) {
        this.listPermission = listPermission;
    }

    public String[] getAction() {
        return action;
    }

    public void setAction(String[] action) {
        this.action = action;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getFilterPermission() {
        return filterPermission;
    }

    public void setFilterPermission(String filterPermission) {
        this.filterPermission = filterPermission;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getRequestMeta() {
        return requestMeta;
    }

    public void setRequestMeta(String requestMeta) {
        this.requestMeta = requestMeta;
    }

    public List<UserViewer> getUserViewer() {
        return userViewer;
    }

    public void setUserViewer(List<UserViewer> userViewer) {
        this.userViewer = userViewer;
    }
    
}
