/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vietdiep.enews.backend.action.adminAction;

import static com.opensymphony.xwork2.Action.INPUT;
import com.opensymphony.xwork2.ActionSupport;
import static com.vietdiep.enews.backend.action.adminAction.AddCategoryAction.requestMeta;

/**
 *
 * @author vietda
 */
public class ManageCategory extends ActionSupport {
    
    static String requestMeta = "";
    
    @Override
    public String execute() throws Exception {
        requestMeta = "manageCategory";
        return SUCCESS;
    } 
}
