/*
 * Copyright 2017 Home Ltd.
 */
package com.vietdiep.enews.backend.common.action;

import com.opensymphony.xwork2.ActionSupport;
import com.vietdiep.enews.bussiness.UserBusiness;
import com.vietdiep.enews.bussiness.common.LoginBussiness;
import com.vietdiep.enews.model.User;
import com.vietdiep.enews.utils.MD5Hash;

import java.util.Random;
//import javax.servlet.http.HttpServletRequest;

/**
 * @author Viet Diep
 */
public class LoginAction extends ActionSupport{

    String userName = "";
    String password = "";
    User user = null;
    String s = "ZXCVBNMASDFGHJKLPOIUYTREWQzxcvbnmlkjhgfdsaqwertyuiop123456789";
    String scode = null;
    String code;
    String errorMessage = null;
    static String tempScode = "";
    int userId;

    private String genarateSecurityCode(String s) {
        String code = null;
        Random random = new Random();
        int index1 = random.nextInt(61) + 0;
        int index2 = random.nextInt(61) + 0;
        int index3 = random.nextInt(61) + 0;
        int index4 = random.nextInt(61) + 0;
        int index5 = random.nextInt(61) + 0;
        int index6 = random.nextInt(61) + 0;
        String num1 = s.substring(index1, index1 + 1);
        String num2 = s.substring(index2, index2 + 1);
        String num3 = s.substring(index3, index3 + 1);
        String num4 = s.substring(index4, index4 + 1);
        String num5 = s.substring(index5, index5 + 1);
        String num6 = s.substring(index6, index6 + 1);
        code = num1 + num2 + num3 + num4 + num5 + num6;
        return code;
    }

    @Override
    public String execute() {
        if (!userName.equalsIgnoreCase("") && !password.equalsIgnoreCase("")) {
            String hashedPassword = LoginBussiness.hashGivenPassword(password);
            userId = LoginBussiness.checkLogin(userName, hashedPassword);
        }
        this.user = UserBusiness.instance.getUserByID(userId);
//        if (user != null && (code.equals(tempScode))) {
        if (this.user != null) {
            if (user.getRole().equalsIgnoreCase("Admin")) {
                return "admin";
            } else if (user.getRole().equalsIgnoreCase("editor")) {
                return "editor";
            } else if (user.getRole().equalsIgnoreCase("reporter")) {
                return "reporter";
            } else if (user.getRole().equalsIgnoreCase("commentapprover")) {
                return "commentapprover";
            }
        } else {
            errorMessage = "code is invalid";
        }
        scode = genarateSecurityCode(s);
        tempScode = scode;
        return INPUT;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getScode() {
        return scode;
    }

    public void setScode(String scode) {
        this.scode = scode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
