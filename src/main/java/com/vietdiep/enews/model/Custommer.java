/*
 * Copyright 2016 Certainty Health
 */
package com.vietdiep.enews.model;

/**
 *
 * @author quangle
 */
public class Custommer {
    String company;
    String contact;
    String country;
    
    public Custommer(String company, String contact, String country){
        this.company=company;
        this.contact=contact;
        this.country=country;
    }
    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
}
