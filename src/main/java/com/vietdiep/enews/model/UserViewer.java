/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vietdiep.enews.model;

import com.vietdiep.enews.utils.DateTimeUtil;
import freemarker.template.utility.DateUtil;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vietda
 */
public class UserViewer {

    public static UserViewer instance = new UserViewer();
    int id;
    String userName;
    String passWord;
    String fullName;
    String email;
    String createDTM;
    int isLock;
    String role;
    String address;
    String phoneNumber;
    String lastUpdateTime;

    public List<UserViewer> getUserViewer(List<User> users) {
        List<UserViewer> userViewers = new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
            UserViewer userViewer = new UserViewer();
            User user = users.get(i);
            userViewer.setAddress(user.getAddress());
            userViewer.setCreateDTM(DateTimeUtil.getStringDateFormat(user.getCreateDTM()));
            userViewer.setEmail(user.getEmail());
            userViewer.setFullName(user.getFullName());
            userViewer.setId(user.getId());
            userViewer.setIsLock(user.isIsLock());
            userViewer.setLastUpdateTime(DateTimeUtil.getStringDateFormat(user.getLastUpdateTime()));
            userViewer.setPassWord(user.getPassWord());
            userViewer.setPhoneNumber(user.getPhoneNumber());
            userViewer.setRole(user.getRole());
            userViewer.setUserName(user.getUserName());
            userViewers.add(userViewer);
        }
        return userViewers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreateDTM() {
        return createDTM;
    }

    public void setCreateDTM(String createDTM) {
        this.createDTM = createDTM;
    }

    public int getIsLock() {
        return isLock;
    }

    public void setIsLock(int isLock) {
        this.isLock = isLock;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(String lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

}
