/*{
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vietdiep.enews.model;

import com.vietdiep.enews.utils.DateTimeUtil;
import freemarker.template.utility.DateUtil;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vietda
 */
public class PostViewer {
    
    public static PostViewer instance = new PostViewer();
    
    int postID;
    int categoryID;
    int userID;
    int views;
    String createdDTM;
    String updatedDTM;
    String publishedTime;
    boolean isHotTop1 = false;
    boolean isHotTop234 = false;
    boolean isHightlight = false;
    String title;
    String shortDescription;
    String content;
    String avatarImage;
    String status;
    
    public List<PostViewer> getListPostViewer(List<Post> posts){
        List<PostViewer> postViewers = new ArrayList<>();
        for(int i = 0; i < posts.size(); i++){
            PostViewer postViewer = new PostViewer();
            Post post = posts.get(i);
            postViewer.setAvatarImage(post.getAvatarImage());
            postViewer.setCategoryID(post.getCategoryID());
            postViewer.setContent(post.getContent());
            postViewer.setCreatedDTM(DateTimeUtil.getStringDateFormatForPost(post.getCreatedDTM()));
            postViewer.setIsHightlight(post.isIsHightlight());
            postViewer.setIsHotTop1(post.isIsHotTop1());
            postViewer.setIsHotTop234(post.isIsHotTop234());
            postViewer.setPostID(post.getPostID());
            postViewer.setPublishedTime(DateTimeUtil.getStringDateFormatForPost(post.getPublishedTime()));
            postViewer.setShortDescription(post.getShortDescription());
            postViewer.setStatus(post.getStatus());
            postViewer.setTitle(post.getTitle());
            postViewer.setUpdatedDTM(DateTimeUtil.getStringDateFormatForPost(post.getUpdatedDTM()));
            postViewer.setUserID(post.getUserID());
            postViewer.setViews(post.getViews());
            postViewers.add(postViewer);
        }
        return postViewers;
    }
    
    public PostViewer getPostViewer(Post post){
            PostViewer postViewer = new PostViewer();
            postViewer.setAvatarImage(post.getAvatarImage());
            postViewer.setCategoryID(post.getCategoryID());
            postViewer.setContent(post.getContent());
            postViewer.setCreatedDTM(DateTimeUtil.getStringDateFormatForPost(post.getCreatedDTM()));
            postViewer.setIsHightlight(post.isIsHightlight());
            postViewer.setIsHotTop1(post.isIsHotTop1());
            postViewer.setIsHotTop234(post.isIsHotTop234());
            postViewer.setPostID(post.getPostID());
            postViewer.setPublishedTime(DateTimeUtil.getStringDateFormatForPost(post.getPublishedTime()));
            postViewer.setShortDescription(post.getShortDescription());
            postViewer.setStatus(post.getStatus());
            postViewer.setTitle(post.getTitle());
            postViewer.setUpdatedDTM(DateTimeUtil.getStringDateFormatForPost(post.getUpdatedDTM()));
            postViewer.setUserID(post.getUserID());
            postViewer.setViews(post.getViews());
        return postViewer;
    }

    public static PostViewer getInstance() {
        return instance;
    }

    public static void setInstance(PostViewer instance) {
        PostViewer.instance = instance;
    }

    public int getPostID() {
        return postID;
    }

    public void setPostID(int postID) {
        this.postID = postID;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public String getCreatedDTM() {
        return createdDTM;
    }

    public void setCreatedDTM(String createdDTM) {
        this.createdDTM = createdDTM;
    }

    public String getUpdatedDTM() {
        return updatedDTM;
    }

    public void setUpdatedDTM(String updatedDTM) {
        this.updatedDTM = updatedDTM;
    }

    public String getPublishedTime() {
        return publishedTime;
    }

    public void setPublishedTime(String publishedTime) {
        this.publishedTime = publishedTime;
    }

    public boolean isIsHotTop1() {
        return isHotTop1;
    }

    public void setIsHotTop1(boolean isHotTop1) {
        this.isHotTop1 = isHotTop1;
    }

    public boolean isIsHotTop234() {
        return isHotTop234;
    }

    public void setIsHotTop234(boolean isHotTop234) {
        this.isHotTop234 = isHotTop234;
    }

    public boolean isIsHightlight() {
        return isHightlight;
    }

    public void setIsHightlight(boolean isHightlight) {
        this.isHightlight = isHightlight;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAvatarImage() {
        return avatarImage;
    }

    public void setAvatarImage(String avatarImage) {
        this.avatarImage = avatarImage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
    
}
