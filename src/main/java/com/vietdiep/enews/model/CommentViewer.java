/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vietdiep.enews.model;

import com.vietdiep.enews.utils.DateTimeUtil;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vietda
 */
public class CommentViewer {

    public static CommentViewer instance = new CommentViewer();

    String authorName;
    String content;
    int commentId;
    int posiId;
    String createdTime;

    public List<CommentViewer> getListCommentViewer(List<Comment> comments) {
        List<CommentViewer> commentViewers = new ArrayList<>();
        if (comments.size() != 0) {
            for (int i = 0; i < comments.size(); i++) {
                CommentViewer commentViewer = new CommentViewer();
                commentViewer.setAuthorName(comments.get(i).getAuthorName());
                commentViewer.setCommentId(comments.get(i).getCommentId());
                commentViewer.setContent(comments.get(i).getContent());
                commentViewer.setCreatedTime(DateTimeUtil.getStringDateFormat(comments.get(i).getCreatedTime()));
                commentViewer.setPosiId(comments.get(i).getPosiId());
                commentViewers.add(commentViewer);
            }
        }
        return commentViewers;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public int getPosiId() {
        return posiId;
    }

    public void setPosiId(int posiId) {
        this.posiId = posiId;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

}
