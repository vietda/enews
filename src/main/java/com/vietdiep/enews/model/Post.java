/*
 * Copyright 2017 Home Ltd.
 */
package com.vietdiep.enews.model;

import java.sql.Timestamp;

/**
 *
 * @author Viet Diep
 */
public class Post {

    int postID;
    int categoryID;
    int userID;
    int views;
    long createdDTM;
    long updatedDTM;
    long publishedTime;
    boolean isHotTop1 = false;
    boolean isHotTop234 = false;
    boolean isHightlight = false;
    String title;
    String shortDescription;
    String content;
    String avatarImage;
    String status;
    
    public int getPostID() {
        return postID;
    }

    public void setPostID(int postID) {
        this.postID = postID;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public long getCreatedDTM() {
        return createdDTM;
    }

    public void setCreatedDTM(long createdDTM) {
        this.createdDTM = createdDTM;
    }

    public long getUpdatedDTM() {
        return updatedDTM;
    }

    public void setUpdatedDTM(long updatedDTM) {
        this.updatedDTM = updatedDTM;
    }

    public boolean isIsHightlight() {
        return isHightlight;
    }

    public void setIsHightlight(boolean isHightlight) {
        this.isHightlight = isHightlight;
    }

    public long getPublishedTime() {
        return publishedTime;
    }

    public void setPublishedTime(long publishedTime) {
        this.publishedTime = publishedTime;
    }

    public boolean isIsHotTop1() {
        return isHotTop1;
    }

    public void setIsHotTop1(boolean isHotTop1) {
        this.isHotTop1 = isHotTop1;
    }

    public boolean isIsHotTop234() {
        return isHotTop234;
    }

    public void setIsHotTop234(boolean isHotTop234) {
        this.isHotTop234 = isHotTop234;
    }

    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAvatarImage() {
        return avatarImage;
    }

    public void setAvatarImage(String avatarImage) {
        this.avatarImage = avatarImage;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
}
