/*
 * Copyright 2017 Home Ltd.
 */
package com.vietdiep.enews.model;

import java.sql.Timestamp;

/**
 *
 * @author Viet Diep
 */
public class User {
    
    int id;
    String userName;
    String passWord;
    String fullName;
    String email;
    long createDTM;
    int isLock;
    String role;
    String address;
    String phoneNumber;
    long lastUpdateTime;
    
    public User() {
        this.isLock = 1;
    }

    public User(int id, String userName, String passWord, String fullName, String email, long createDTM, int status, String role){
        this.id = id;
        this.createDTM = createDTM;
        this.email = email;
        this.fullName = fullName;
        this.isLock = status;
        this.passWord = passWord;
        this.userName = userName;
        this.role = this.role;
    }
 
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getCreateDTM() {
        return createDTM;
    }

    public void setCreateDTM(long createDTM) {
        this.createDTM = createDTM;
    }

    public int isIsLock() {
        return isLock;
    }

    public void setIsLock(int status) {
        this.isLock = status;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public long getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(long lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }
    
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
}
