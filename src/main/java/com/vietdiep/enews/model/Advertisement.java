/*
 * Copyright 2017 Home Ltd.
 */
package com.vietdiep.enews.model;

import java.sql.Timestamp;

/**
 *
 * @author Viet Diep
 */
public class Advertisement {
    
    int adsID;
    int customerID;
    int PositionID;
    int AproverID;
    String avatarImage;
    String link;
    int totalClick;
    int totalViews;
    Timestamp startDTM;
    Timestamp endDTM;
    Timestamp createdDTM;
    boolean isTrash;
    boolean isLock;

    public int getAdsID() {
        return adsID;
    }

    public void setAdsID(int adsID) {
        this.adsID = adsID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public int getPositionID() {
        return PositionID;
    }

    public void setPositionID(int PositionID) {
        this.PositionID = PositionID;
    }

    public int getAproverID() {
        return AproverID;
    }

    public void setAproverID(int AproverID) {
        this.AproverID = AproverID;
    }

    public String getAvatarImage() {
        return avatarImage;
    }

    public void setAvatarImage(String avatarImage) {
        this.avatarImage = avatarImage;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getTotalClick() {
        return totalClick;
    }

    public void setTotalClick(int totalClick) {
        this.totalClick = totalClick;
    }

    public int getTotalViews() {
        return totalViews;
    }

    public void setTotalViews(int totalViews) {
        this.totalViews = totalViews;
    }
    
    

    public Timestamp getStartDTM() {
        return startDTM;
    }

    public void setStartDTM(Timestamp startDTM) {
        this.startDTM = startDTM;
    }

    public Timestamp getEndDTM() {
        return endDTM;
    }

    public void setEndDTM(Timestamp endDTM) {
        this.endDTM = endDTM;
    }

    public Timestamp getCreatedDTM() {
        return createdDTM;
    }

    public void setCreatedDTM(Timestamp createdDTM) {
        this.createdDTM = createdDTM;
    }

    public boolean isIsTrash() {
        return isTrash;
    }

    public void setIsTrash(boolean isTrash) {
        this.isTrash = isTrash;
    }

    public boolean isIsLock() {
        return isLock;
    }

    public void setIsLock(boolean isLock) {
        this.isLock = isLock;
    }
    
    
}
