/*
 * Copyright 2017 Home Ltd.
 */
package com.vietdiep.enews.model;

/**
 *
 * @author Viet Diep
 */
public class CategoryNameAndParentCategoryName {
    
    String categoryName;
    String parentCategoryName;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getParentCategoryName() {
        return parentCategoryName;
    }

    public void setParentCategoryName(String parentCategoryName) {
        this.parentCategoryName = parentCategoryName;
    }
}
